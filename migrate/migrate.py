#!/usr/bin/env python3

# the names are supposed to be in format LEVEL_(apply|revert)_NAME.sql
# every level has to have an apply file, but a revert is not needed

import sys
import glob
import os 
import subprocess 

DBCOMMAND = "psql"
DUMPCOMMAND = "pg_dump --schema-only"

if len(sys.argv) < 2:
    print("USAGE: ./{} DBNAME [TARGET]".format(sys.argv[0]))
    sys.exit(1)
else:
    dbname = sys.argv[1]

target = None
if(len(sys.argv)) > 2:
    target = int(sys.argv[2])

test_cmd = "{} {}".format(DBCOMMAND, dbname)

try:
    f = open('./migrate/level.lock')
    curr = int(f.read())
    f.close()
except:
    print("could not open the lock file.")
    curr = 0

files = glob.glob('./migrate/*.sql')
# { level => { "apply": apply_filename, "revert": revert_filename } }
processed = dict()

for full in files: 
    fileext = os.path.split(full)[1]
    file = os.path.splitext(fileext)[0]
    parts = file.split("_")
    if len(parts) == 1: continue
    level = int(parts[0])
    op = parts[1]
    if level not in processed:
        processed[level] = dict()
    processed[level][op] = full

levels = sorted(list(processed.keys()))

if target is None:
    target = levels[-1]

if target == curr:
    print("No migrations run")
    sys.exit(0)
elif target > curr:
    for level in levels:
        if level <= curr: continue
        file = processed[level]["apply"]
        cmd = "{} {} < {}".format(DBCOMMAND, dbname, file)
        print("Running {}".format(cmd))
        os.system(cmd)
else:
    # target < curr:
    for level in reversed(levels):
        if level > curr: continue
        if "revert" in processed[level]:
            file = processed[level]["revert"]
            cmd = "{} {} < {}".format(DBCOMMAND, dbname, file)
            print("Running {}".format(cmd))
            os.system(cmd)

cmd = "{} {} > {}".format(DUMPCOMMAND, dbname, "migrate/schema.sql")
print("Running {}".format(cmd))
os.system(cmd)

with open("./migrate/level.lock", "w") as f: 
    f.write(str(target))