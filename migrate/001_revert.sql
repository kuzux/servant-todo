drop index ix_boards_user_id;
drop index ix_items_board_id;

drop table items;
drop table boards;
drop table users;

drop extension pgcrypto;