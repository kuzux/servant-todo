create extension pgcrypto;

create table users(
    id uuid primary key default gen_random_uuid(),
    name text not null,
    email text not null,
    created_at timestamp not null default now(),
    updated_at timestamp not null default now(),
    deleted_at timestamp default null
);

create table boards(
    id uuid primary key default gen_random_uuid(),
    name text not null,
    user_id uuid not null references users(id),
    created_at timestamp not null default now(),
    updated_at timestamp not null default now(),
    deleted_at timestamp default null
);

create table items(
    id uuid primary key default gen_random_uuid(),
    content text not null,
    board_id uuid not null references boards(id),
    order_value integer not null,
    created_at timestamp not null default now(),
    updated_at timestamp not null default now(),
    deleted_at timestamp default null
);

create index ix_boards_user_id on boards (user_id);
create index ix_items_board_id on items (board_id);