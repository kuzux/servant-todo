alter table items drop column updated_at;
alter table boards drop column updated_at;
alter table users drop column updated_at;