alter table items add column timestamp not null default now();
alter table boards add column timestamp not null default now();
alter table users add column timestamp not null default now();