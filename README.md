# servant-todo

Essentially a simple server-side application to teacxh myself servant and developing web applications on haskell. The main program is a glorified todo list partially inspired by TodoMVC. Though, unlike TodoMVC; has more complicated authenication/multi-user logic and more complex relations betweemn the records (commonly found in webapps in the wild). 

The application lacks an actual frontend for now; only an imagnary client for the API exists.

It is intentionally a monolith; I'm very inexperienced in building production applications in haskell; no need to introduce extra complexity via microservices and all that. For that same reason (Me being very inexperienced); I can imagine that there are some fairly obvious flaws in the code, and any criticism/contribution is welcome.

The current main dependency for the program is `stack`. Any other haskell libraries requirted will be installed by it (After running `stack build`). The test and migration scripts are written in python3 and they require it to be run. The python scripts depend on `pgpsyco2`, `requests` and `faker`

To run the application; you need to have a postgres database running on your local machine named `servant-thing` and you need to run `python3 migrate/migrate.py` to run the database migrations. After the database migrations are run; you can run `python3 test/runnner.py` to run the integration tests. Then just run `servant-todo-exe` in the main directory to run the server.
