#!/usr/bin/python3

# These tests are not unit tests, those are end-to-end tests
# They essentially simulate running a bunch of queries by hand and are very
# black box tests, they have no idea what the server itself is implemented in
# So they are intentionally in a different language

import cases
import faker
import os
import psycopg2
import shlex
import subprocess
import random
import requests
import sys
import time

DBNAME = "servant_thing"
EXECNAME = "servant-todo-exe"

def server_command(name):
    which_cmd = "stack exec -- which {}".format(EXECNAME)
    p = subprocess.Popen(shlex.split(which_cmd), stdout=subprocess.PIPE)
    filename = str(p.communicate()[0].strip(), 'utf-8')
    return "{} config.toml".format(filename)

def truncate_db(conn):
    cur = conn.cursor()
    cur.execute("truncate table items cascade")
    cur.execute("truncate table boards cascade")
    cur.execute("truncate table users cascade")
    cur.close()
    conn.commit()

def generate_data(conn):
    NUM_USERS = 100
    NUM_BOARDS = 300
    NUM_ITEMS = 3500

    user_ids = []
    board_ids = []
    item_ids = []

    board_users = dict()
    item_boards = dict()

    fake = faker.Faker()

    for i in range(NUM_USERS):
        cur = conn.cursor()
        cur.execute("insert into users (name, email) values (%s, %s) returning id",
            (fake.name(), fake.email()))
        user_ids.append(cur.fetchone()[0])
        cur.close()
    conn.commit()

    for i in range(NUM_BOARDS):
        uid = random.choice(user_ids)
        cur = conn.cursor()
        cur.execute("insert into boards (name, user_id) values (%s, %s) returning id",
            (fake.name(), uid))

        bid = cur.fetchone()[0]
        board_ids.append(bid)
        board_users[bid] = uid
        cur.close()
    conn.commit()

    for i in range(NUM_ITEMS):
        bid = random.choice(board_ids)
        cur = conn.cursor()
        cur.execute("insert into items (content, board_id) values (%s, %s) returning id",
            (fake.sentence(), bid))

        iid = cur.fetchone()[0]
        item_ids.append(iid)
        item_boards[iid] = bid
        cur.close()
    conn.commit()

    return { "user_ids": user_ids, "board_ids": board_ids, "item_ids": item_ids, 
        "board_users": board_users, "item_boards": item_boards }

def login_all(ctx):
    port = 5000

    ctx['user_tokens'] = dict()

    for uid in ctx['user_ids']:
        url = "http://localhost:{}/login".format(port)
        response = requests.post(url,
            json={
                "token": "totally-valid-token",
                "provider": "google",
                "user_id": uid
            })
        ctx['user_tokens'][uid] = response.text

if __name__ == "__main__":
    start_tm = time.perf_counter()
    print("Compiling server...", end=" ")
    compiler_retval = subprocess.call(["stack", "build"])
    end_tm = time.perf_counter()
    print("Done (in {:.2f}s)".format(end_tm-start_tm))
    if compiler_retval != 0:
        print()
        print("Compiler returned error")
        sys.exit(1)

    start_tm = time.perf_counter()
    print("Generating data...", end=" ")
    conn = psycopg2.connect("dbname={}".format(DBNAME))
    truncate_db(conn)
    cases.suite.ctx = generate_data(conn)
    end_tm = time.perf_counter()
    print("Done (in {:.2f}s)".format(end_tm-start_tm))

    command = server_command(EXECNAME)
    server_process = subprocess.Popen(shlex.split(command), 
        shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    end_tm = time.perf_counter()

    # Waiting for server to start accepting requests
    time.sleep(0.1)

    start_tm = time.perf_counter()
    print("Logging in...", end=" ")
    login_all(cases.suite.ctx)
    end_tm = time.perf_counter()
    print("Done (in {:.2f}s)".format(end_tm-start_tm))

    start_tm = time.perf_counter()
    print("Running tests...")
    print()
    try:
        test_result = cases.suite.run_tests()
        cases.suite.print_explanation()
        print()
    except e: 
        print(e)

    end_tm = time.perf_counter()
    print("Done (in {:.2f}s)".format(end_tm-start_tm))

    server_process.terminate()
    returncode = server_process.wait()

    server_stderr = str(server_process.stderr.read(), 'utf-8')

    if server_stderr != "":
        print("Server process errors:")
        print(server_stderr)

    if not test_result:
        sys.exit(1)
