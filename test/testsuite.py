import inspect
import traceback

class PendingError(Exception):
    pass

class TestSuite(object):
    def __init__(self, name):
        self.suite_name = name

        self.testcases = []
        self.results = []
        self.before_each_cbs = []
        self.before_all_cbs = []
        
        self.ctx = None

        self.error_count = 0
        self.pending_count = 0
        self.pass_count = 0

    def test(self, name):
        def decorator(fn):
            fnname = fn.__name__
            filename = inspect.getsourcefile(fn)
            lineno = inspect.currentframe().f_back.f_lineno
            location = "{} at {}:{}".format(fnname, filename, lineno)
            self.testcases.append((name, fn, location))
        return decorator

    def pending(self, name):
        def thrower(ctx):
            raise PendingError

        def decorator(fn):
            fnname = fn.__name__
            filename = inspect.getsourcefile(fn)
            lineno = inspect.currentframe().f_back.f_lineno
            location = "{} at {}:{}".format(fnname, filename, lineno)
            self.testcases.append((name, thrower, location))
        return decorator
    
    def before_each(self, fn):
        self.before_each_cbs.append(fn)

    def before_all(self, fn):
        self.before_all_cbs.append(fn)
    
    def run_tests(self):
        print("\u001b[0m", end="")
        print("{} - Results:".format(self.suite_name))
        
        for fn in self.before_all_cbs:
                fn()
        for (name, case, location) in self.testcases:
            for fn in self.before_each_cbs:
                fn()
            try:
                case(self.ctx)
                print("\u001b[32m", end="")
                print(".", end="", flush=True)
                self.pass_count += 1
                self.results.append(True)
            except AssertionError as err:
                print("\u001b[31m", end="")
                print("F", end="", flush=True)
                self.error_count += 1
                trace = traceback.format_exc()
                self.results.append((name, "fail", trace, location))
            except PendingError as err:
                print("\u001b[33m", end="")
                print("*", end="", flush=True)
                self.pending_count += 1
                self.results.append((name, "pending", "", location))
            except Exception as err:
                print("\u001b[31m", end="")
                print("F", end="", flush=True)
                self.error_count += 1
                trace = traceback.format_exc()
                self.results.append((name, "error", trace, location))
        print("\u001b[0m", end="")
        print()
        return (self.error_count == 0)

    def print_explanation(self):
        print("\u001b[0m", end="")
        print()
        print("{} - Report:".format(self.suite_name))
        if self.pending_count != 0:
            print("\u001b[33m", end="")
            print("Pending:")
            index = 1
            for result in self.results:
                if result == True: continue
                (name, typ, msg, loc) = result
                if typ == "pending": 
                    explanation = "Test Pending"
                    print("{}) {} in {}".format(index, explanation, name))
                    print("    ({})".format(loc))
                    index += 1

        if self.error_count != 0:
            print("\u001b[31m", end="")
            print("Errors:")
            index = 1
            for result in self.results:
                if result == True: continue
                (name, typ, msg, loc) = result
                if typ == "pending": continue
                if typ == "fail": explanation = "Assertion Failure"
                else: explanation = "Exception Caught"
                print("{}) {} in {}".format(index, explanation, name))
                print("    ({})".format(loc))
                print(msg)
                index += 1
        else:
            print("\u001b[32m", end="")
            print("OK ({} test cases passed)".format(self.pass_count))

        print("\u001b[0m", end="")
        return (self.error_count == 0)
