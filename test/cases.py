import faker
import random
import requests
import uuid

from testsuite import * 

PORT = 5000
suite = TestSuite("Acceptance Tests")

def base_url(port):
    return "http://localhost:{}".format(port)

def login_as(ctx, uid):
    return {
        "Authorization": "Bearer {}".format(ctx['user_tokens'][uid])
    }

def login_as_random(ctx):
    uid = random.choice(ctx['user_ids'])
    return login_as(ctx, uid)

@suite.test("List Users Response Shape")
def test_list_users(ctx):
    url = "{}/users".format(base_url(PORT))

    response = requests.get(url, headers=login_as_random(ctx))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    random_user = random.choice(json['data'])
    assert 'id' in random_user
    assert 'name' in random_user
    assert 'email' in random_user

    actual = sorted(map(lambda u: u['id'], json['data']))
    expected = sorted(ctx['user_ids'])
    assert actual == expected

@suite.test("Get Users Response")
def test_get_user(ctx):
    random_uid = random.choice(ctx['user_ids'])
    url = "{}/users/{}".format(base_url(PORT), random_uid)

    response = requests.get(url, headers=login_as_random(ctx))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    assert 'id' in json['data']
    assert 'name' in json['data']
    assert 'email' in json['data']

    assert json['data']['id'] == random_uid

@suite.test("Get Invalid User")
def test_get_invalid_user(ctx):
    # very unlikely to have a collision with an actual user in the db
    random_uid = uuid.uuid4()

    url = "{}/users/{}".format(base_url(PORT), random_uid)
    response = requests.get(url, headers=login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'
    assert 'data' not in json

@suite.test("Add User and Try to List Users")
def test_add_user_and_list(ctx):
    fake = faker.Faker()
    payload = {
        "name": fake.name(),
        "email": fake.email()
    }

    url = "{}/signup".format(base_url(PORT))
    response = requests.post(url, json=payload)
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    added_uid = json['data']
    # We don't really need to do this for users we've added
    # ctx['user_ids'].append(added_uid)

    url = "{}/users".format(base_url(PORT))
    response = requests.get(url, headers=login_as_random(ctx))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    response_uids = map(lambda u: u['id'], json['data'])
    assert added_uid in response_uids

@suite.test("Add User and Try to Get User")
def test_add_user_and_get(ctx):
    fake = faker.Faker()
    payload = {
        "name": fake.name(),
        "email": fake.email()
    }

    url = "{}/signup".format(base_url(PORT))
    response = requests.post(url, json=payload)
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    added_uid = json['data']
    # ctx['user_ids'].append(added_uid)

    url = "{}/users/{}".format(base_url(PORT), added_uid)
    response = requests.get(url, headers=login_as_random(ctx))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'
    assert json['data']['id'] == added_uid

@suite.test("Add Invalid User")
def test_add_invalid_user(ctx):
    fake = faker.Faker()
    payload = {
        "name": fake.name(),
        "emaik": fake.email()
    }

    url = "{}/signup".format(base_url(PORT))
    response = requests.post(url, json=payload)
    assert response.status_code != 200

    payload = {
        "name": fake.name(),
    }

    url = "{}/users".format(base_url(PORT))
    response = requests.post(url, json=payload, headers=login_as_random(ctx))
    assert response.status_code != 200

@suite.test("Delete User and Try to List Users")
def test_delete_user_and_list(ctx):
    random_uid = random.choice(ctx['user_ids'])

    url = "{}/users/{}".format(base_url(PORT), random_uid)
    response = requests.delete(url,
        headers=login_as(ctx, random_uid))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    ctx['user_ids'].remove(random_uid)

    url = "{}/users/".format(base_url(PORT))
    response = requests.get(url, headers=login_as_random(ctx))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    response_uids = map(lambda u: u['id'], json['data'])
    assert random_uid not in response_uids

@suite.test("Delete User and Try to Get User")
def test_delete_user_and_get(ctx):
    random_uid = random.choice(ctx['user_ids'])
    
    url = "{}/users/{}".format(base_url(PORT), random_uid)
    response = requests.delete(url,
        headers=login_as(ctx, random_uid))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    ctx['user_ids'].remove(random_uid)

    url = "{}/users/{}".format(base_url(PORT), random_uid)
    response = requests.get(url, headers=login_as_random(ctx))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'
    assert 'data' not in json

@suite.test("Try to Delete while Logged Out")
def test_delete_user_logged_out(ctx):
    random_uid = random.choice(ctx['user_ids'])

    url = "{}/users/{}".format(base_url(PORT), random_uid)
    response = requests.delete(url)
    assert response.status_code == 401

@suite.test("Try to Delete Nonexistent User")
def test_delete_invalid_user(ctx):
    random_uid = uuid.uuid4()

    url = "{}/users/{}".format(base_url(PORT), random_uid)
    response = requests.delete(url,
        headers=login_as_random(ctx))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'
    assert 'data' not in json

@suite.test("Try to Delete Some Other User")
def test_delete_other_user(ctx):
    logged_in = random.choice(ctx['user_ids'])

    ctx['user_ids'].remove(logged_in)
    try_to_delete = random.choice(ctx['user_ids'])
    ctx['user_ids'].append(logged_in)

    url = "{}/users/{}".format(base_url(PORT), try_to_delete)
    response = requests.delete(url,
        headers=login_as(ctx, logged_in))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'
    assert 'data' not in json

@suite.test("Add User and Find by Email")
def test_add_user_and_find_by_email(ctx):
    fake = faker.Faker()
    added_email = fake.email()
    payload = {
        "name": fake.name(),
        "email": added_email
    }

    url = "{}/signup".format(base_url(PORT))
    response = requests.post(url, json=payload)
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    added_uid = json['data']
    # ctx['user_ids'].append(added_uid)

    url = "{}/users/by-email/{}".format(base_url(PORT), added_email)
    response = requests.get(url, headers=login_as_random(ctx))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'
    assert json['data']['id'] == added_uid

    assert 'id' in json['data']
    assert 'email' in json['data']
    assert 'name' in json['data']
    assert 'created_at' in json['data']

@suite.test("Find User By Invalid Email")
def test_find_user_by_invalid_email(ctx):
    # A uuid is never a valid mail address
    random_uid = uuid.uuid4()

    url = "{}/users/by-email/{}".format(base_url(PORT), random_uid)
    response = requests.get(url, headers=login_as_random(ctx))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'
    assert 'data' not in json

@suite.test("List Boards by Random Valid User")
def test_list_boards_by_user(ctx):
    random_bid = random.choice(ctx['board_ids'])
    random_uid = ctx['board_users'][random_bid]

    url = "{}/boards/by-user/{}".format(base_url(PORT), random_uid)
    response = requests.get(url, headers=login_as_random(ctx))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'
    assert type(json['data']) is list

    # We know there is at least one board
    assert 'id' in json['data'][0]
    assert 'name' in json['data'][0]
    assert 'user_id' in json['data'][0]
    assert 'created_at' in json['data'][0]

@suite.test("List Boards by Random Invalid User")
def test_list_boards_by_invalid_user(ctx):
    random_uid = uuid.uuid4()

    url = "{}/boards/by-user/{}".format(base_url(PORT), random_uid)
    response = requests.get(url, headers=login_as_random(ctx))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'
    assert 'data' not in json

@suite.test("Delete User, Try to Add a Board")
def test_add_board_deleted_user(ctx):
    random_uid = random.choice(ctx['user_ids'])
    fake = faker.Faker()

    url = "{}/users/{}".format(base_url(PORT), random_uid)
    response = requests.delete(url, headers=login_as(ctx, random_uid))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    ctx['user_ids'].remove(random_uid)

    url = "{}/boards".format(base_url(PORT))
    response = requests.post(url,
        headers = login_as(ctx, random_uid),
        json = {
            "name": fake.sentence(),
            "user_id": str(random_uid)
        })

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'

@suite.test("Add a Board, List Boards")
def test_add_and_list_board(ctx):
    random_uid = random.choice(ctx['user_ids'])
    fake = faker.Faker()

    url = "{}/boards".format(base_url(PORT))
    response = requests.post(url,
        headers = login_as(ctx, random_uid),
        json = {
            "name": fake.sentence(),
            "user_id": str(random_uid)
        })

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    added_bid = json['data']
    ctx['board_ids'].append(added_bid)
    ctx['board_users'][added_bid] = random_uid

    url = "{}/boards/by-user/{}".format(base_url(PORT), random_uid)
    response = requests.get(url, headers=login_as_random(ctx))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    assert added_bid in map(lambda e: e['id'], json['data'])

@suite.test("Add a Board, Delete it")
def test_add_board_delete(ctx):
    random_uid = random.choice(ctx['user_ids'])
    fake = faker.Faker()

    url = "{}/boards".format(base_url(PORT))
    response = requests.post(url,
        headers = login_as(ctx, random_uid),
        json = {
            "name": fake.sentence(),
            "user_id": str(random_uid)
        })
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    added_bid = json['data']

    url = "{}/boards/{}".format(base_url(PORT), added_bid)
    response = requests.delete(url,
        headers=login_as(ctx, random_uid))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

@suite.test("Try Deleting a Nonexistent Board")
def test_delete_nonexistent_board(ctx):
    random_uid = random.choice(ctx['user_ids'])
    random_bid = uuid.uuid4()

    url = "{}/boards/{}".format(base_url(PORT), random_bid)
    response = requests.delete(url,
        headers=login_as(ctx, random_uid))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'

@suite.test("Try Deleting the Board of Another User")
def test_delete_other_users_board(ctx):
    random_bid = random.choice(ctx['board_ids'])

    url = "{}/boards/{}".format(base_url(PORT), random_bid)
    response = requests.delete(url, headers=login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'

@suite.test("List items in a board")
def test_list_items_by_board(ctx):
    random_bid = random.choice(ctx['board_ids'])

    url = "{}/items/by-board/{}".format(base_url(PORT), random_bid)
    response = requests.get(url, headers=login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

@suite.test("List items in a nonexistent board")
def test_list_items_by_nonexistent_board(ctx):
    random_bid = uuid.uuid4()

    url = "{}/items/by-board/{}".format(base_url(PORT), random_bid)
    response = requests.get(url, headers=login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'

@suite.test("List items in a deleted board")
def test_list_items_by_deleted_board(ctx):
    # Should still work
    random_bid = random.choice(ctx['board_ids'])
    uid = ctx['board_users'][random_bid]

    url = "{}/boards/{}".format(base_url(PORT), random_bid)
    response = requests.delete(url,
        headers=login_as(ctx, uid))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    ctx['board_ids'].remove(random_bid)

    url = "{}/items/by-board/{}".format(base_url(PORT), random_bid)
    response = requests.get(url, headers=login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

@suite.test("List items by a user")
def test_list_items_by_user(ctx):
    random_bid = random.choice(ctx['user_ids'])

    url = "{}/items/by-user/{}".format(base_url(PORT), random_bid)
    response = requests.get(url, headers=login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

@suite.test("List items by a nonexistent user")
def test_list_items_by_nonexistent_user(ctx):
    random_bid = uuid.uuid4()

    url = "{}/items/by-user/{}".format(base_url(PORT), random_bid)
    response = requests.get(url, headers=login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'

@suite.test("List items by a deleted user")
def test_list_items_by_deleted_user(ctx):
    # Should still work
    random_uid = random.choice(ctx['user_ids'])

    url = "{}/users/{}".format(base_url(PORT), random_uid)
    response = requests.delete(url,
        headers=login_as(ctx, random_uid))
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    ctx['user_ids'].remove(random_uid)

    url = "{}/items/by-user/{}".format(base_url(PORT), random_uid)
    response = requests.get(url, headers=login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

@suite.test("Listed items have the correct shape")
def test_list_items_shape(ctx):
    random_iid = random.choice(ctx['item_ids'])
    bid = ctx['item_boards'][random_iid]

    url = "{}/items/by-board/{}".format(base_url(PORT), bid)
    response = requests.get(url, headers=login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'
    
    assert 'id' in json['data'][0]
    assert 'name' in json['data'][0]
    assert 'completed' in json['data'][0]
    assert 'board_id' in json['data'][0]
    assert 'created_at' in json['data'][0]

@suite.test("Item is listed under its board")
def test_item_listed_by_board(ctx):
    random_iid = random.choice(ctx['item_ids'])
    bid = ctx['item_boards'][random_iid]

    url = "{}/items/by-board/{}".format(base_url(PORT), bid)
    response = requests.get(url, headers=login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    ids = map(lambda e: e['id'], json['data'])
    assert random_iid in ids

@suite.test("Listed items (by user) have the correct shape")
def test_list_items_by_user_shape(ctx):
    random_iid = random.choice(ctx['item_ids'])
    bid = ctx['item_boards'][random_iid]
    uid = ctx['board_users'][bid]

    url = "{}/items/by-user/{}".format(base_url(PORT), uid)
    response = requests.get(url, headers=login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'
    
    assert 'id' in json['data'][0]
    assert 'name' in json['data'][0]
    assert 'completed' in json['data'][0]
    assert 'board_id' in json['data'][0]
    assert 'created_at' in json['data'][0]

@suite.test("Item is listed under its user")
def test_item_listed_by_user(ctx):
    random_iid = random.choice(ctx['item_ids'])
    bid = ctx['item_boards'][random_iid]
    uid = ctx['board_users'][bid]

    url = "{}/items/by-user/{}".format(base_url(PORT), uid)
    response = requests.get(url, headers=login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    ids = map(lambda e: e['id'], json['data'])
    assert random_iid in ids

@suite.test("Update board and try to read it")
def test_update_board(ctx):
    random_bid = random.choice(ctx['board_ids'])
    fake = faker.Faker()
    uid = ctx['board_users'][random_bid]

    newname = fake.sentence()

    url = "{}/boards/{}".format(base_url(PORT), random_bid)
    response = requests.put(url,
        headers = login_as(ctx, uid),
        json = {
            "name": newname,
            "user_id": str(uid)
        })

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    url = "{}/boards/by-user/{}".format(base_url(PORT), uid)
    response = requests.get(url,
        headers = login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    board = next(filter(lambda e: e['id'] == random_bid,
        json['data']))

    assert board['name'] == newname
    assert board['user_id'] == str(uid)

@suite.test("Try to update someone else's board")
def test_update_board_unauth(ctx):
    random_bid = random.choice(ctx['board_ids'])
    fake = faker.Faker()
    uid = ctx['board_users'][random_bid]

    random_uid = random.choice(ctx['user_ids'])
    if random_uid == uid:
        random_uid = random.choice(ctx['user_ids'])

    url = "{}/boards/{}".format(base_url(PORT), random_bid)
    response = requests.put(url,
        headers = login_as(ctx, random_uid),
        json = {
            "name": fake.sentence(),
            "user_id": str(uid)
        })

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'

@suite.test("Try to update a nonexistent board")
def test_update_nonexistent_board(ctx):
    random_bid = uuid.uuid4()
    fake = faker.Faker()
    random_uid = random.choice(ctx['user_ids'])

    url = "{}/boards/{}".format(base_url(PORT), random_bid)
    response = requests.put(url,
        headers = login_as_random(ctx),
        json = {
            "name": fake.sentence(),
            "user_id": str(random_uid)
        })

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'

@suite.test("Try to update board with invalid input")
def test_update_board_invalid(ctx):
    random_bid = random.choice(ctx['board_ids'])
    fake = faker.Faker()
    uid = ctx['board_users'][random_bid]

    newname = fake.sentence()

    url = "{}/boards/{}".format(base_url(PORT), random_bid)
    response = requests.put(url,
        headers = login_as(ctx, uid),
        json = {
            "name": newname,
            "sdfds": str(uid)
        })

    assert response.status_code != 200

@suite.test("Delete board and try to read it")
def test_delete_board(ctx):
    random_bid = random.choice(ctx['board_ids'])
    fake = faker.Faker()
    uid = ctx['board_users'][random_bid]

    url = "{}/boards/{}".format(base_url(PORT), random_bid)
    response = requests.delete(url,
        headers = login_as(ctx, uid))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    ctx['board_ids'].remove(random_bid)
    del ctx['board_users'][random_bid]

    url = "{}/boards/by-user/{}".format(base_url(PORT), uid)
    response = requests.get(url,
        headers = login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    board_ids = map(lambda e: e['id'], json['data'])
    assert random_bid not in board_ids

@suite.test("Try to delete someone else's board")
def test_delete_board_unauth(ctx):
    random_bid = random.choice(ctx['board_ids'])
    fake = faker.Faker()
    uid = ctx['board_users'][random_bid]

    random_uid = random.choice(ctx['user_ids'])
    if random_uid == uid:
        random_uid = random.choice(ctx['user_ids'])

    url = "{}/boards/{}".format(base_url(PORT), random_bid)
    response = requests.delete(url,
        headers = login_as(ctx, random_uid))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'

@suite.test("Try to delete a nonexistent board")
def test_delete_nonexistent_board(ctx):
    random_bid = uuid.uuid4()
    fake = faker.Faker()
    random_uid = random.choice(ctx['user_ids'])

    url = "{}/boards/{}".format(base_url(PORT), random_bid)
    response = requests.delete(url,
        headers = login_as(ctx, random_uid))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'

@suite.test("Update item and try to read it")
def test_update_item(ctx):
    random_iid = random.choice(ctx['item_ids'])
    fake = faker.Faker()
    bid = ctx['item_boards'][random_iid]
    uid = ctx['board_users'][bid]

    newname = fake.sentence()

    url = "{}/items/{}".format(base_url(PORT), random_iid)
    response = requests.put(url,
        headers = login_as(ctx, uid),
        json = {
            "name": newname,
            "completed": True,
            "board_id": str(bid)
        })

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    url = "{}/items/{}".format(base_url(PORT), random_iid)
    response = requests.get(url,
        headers = login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    assert json['data']['name'] == newname
    assert json['data']['board_id'] == str(bid)
    assert json['data']['completed'] == True

@suite.test("Try to update someone else's item")
def test_update_item_unauth(ctx):
    random_iid = random.choice(ctx['item_ids'])
    fake = faker.Faker()
    bid = ctx['item_boards'][random_iid]
    uid = ctx['board_users'][bid]

    newname = fake.sentence()

    random_uid = random.choice(ctx['user_ids'])
    if random_uid == uid:
        random_uid = random.choice(ctx['user_ids'])

    url = "{}/items/{}".format(base_url(PORT), random_iid)
    response = requests.put(url,
        headers = login_as(ctx, random_uid),
        json = {
            "name": newname,
            "completed": True,
            "board_id": str(bid)
        })

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'

@suite.test("Try to update item with invalid input")
def test_update_item_invalid(ctx):
    random_iid = random.choice(ctx['item_ids'])
    fake = faker.Faker()
    bid = ctx['item_boards'][random_iid]
    uid = ctx['board_users'][bid]

    newname = fake.sentence()

    url = "{}/items/{}".format(base_url(PORT), random_iid)
    response = requests.put(url,
        headers = login_as(ctx, uid),
        json = {
            "name": newname,
            "sdfds": True,
            "board_id": str(bid)
        })

    assert response.status_code != 200

@suite.test("Delete item and try to read it")
def test_delete_item(ctx):
    random_iid = random.choice(ctx['item_ids'])
    fake = faker.Faker()
    bid = ctx['item_boards'][random_iid]
    uid = ctx['board_users'][bid]

    url = "{}/items/{}".format(base_url(PORT), random_iid)
    response = requests.delete(url,
        headers = login_as(ctx, uid))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'success'

    url = "{}/items/{}".format(base_url(PORT), random_iid)
    response = requests.get(url,
        headers = login_as_random(ctx))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'

@suite.test("Try to delete someone else's item")
def test_delete_item_unauth(ctx):
    random_iid = random.choice(ctx['item_ids'])
    fake = faker.Faker()
    bid = ctx['item_boards'][random_iid]
    uid = ctx['board_users'][bid]

    random_uid = random.choice(ctx['user_ids'])
    if random_uid == uid:
        random_uid = random.choice(ctx['user_ids'])

    url = "{}/items/{}".format(base_url(PORT), random_iid)
    response = requests.delete(url,
        headers = login_as(ctx, random_uid))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'

@suite.test("Try to delete a nonexistent item")
def test_delete_nonexistent_item(ctx):
    random_bid = uuid.uuid4()
    fake = faker.Faker()
    random_uid = random.choice(ctx['user_ids'])

    url = "{}/boards/{}".format(base_url(PORT), random_bid)
    response = requests.delete(url,
        headers = login_as(ctx, random_uid))

    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'error'
