{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module DB 
    ( Connection
    , CachedConnection
    , Logged
    
    , query
    , query_
    , execute

    , cachedQuery
    , cachedQuery_
    , uncachedQuery
    , uncachedExecute

    , mkCached
    , mkLogged
    , mkPostgresPool
    ) where

import           Control.Monad.IO.Class (liftIO)
import qualified Data.Aeson as Ae
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import           Data.Int (Int64)
import           Data.Maybe (fromJust)
import           Data.Pool (Pool, createPool, withResource)
import qualified Database.PostgreSQL.Simple as PG
import qualified Database.Redis as Rd
import qualified System.Log.FastLogger as FL
import           System.Log.FastLogger.Date (newTimeCache, simpleTimeFormat)

class Connection conn where
    query :: (PG.ToRow q, PG.FromRow r) => 
         conn -> PG.Query -> q -> IO [r]
    query_ :: (PG.FromRow r) => conn -> PG.Query -> IO [r]
    query_ conn expr = query conn expr ()
    execute :: (PG.ToRow q) => conn -> PG.Query -> q -> IO Int64    
    formatQuery :: (PG.ToRow q) => conn -> PG.Query -> q -> IO BS.ByteString

type CacheKey = BS.ByteString

class Connection cconn => CachedConnection cconn where 
    cachedQuery     :: (PG.ToRow q, PG.FromRow r, Ae.ToJSON s, Ae.FromJSON t) => cconn
        -> PG.Query -> CacheKey -> q -> ([r] -> u) -> ([r] -> Maybe s) -> (t -> u) -> IO u
    cachedQuery_    :: (PG.FromRow r, Ae.ToJSON s, Ae.FromJSON t) => cconn
        -> PG.Query -> CacheKey -> ([r] -> u) -> ([r] -> Maybe s) -> (t -> u) -> IO u
    cachedQuery_ cconn expr key dbMapper dbToCache cacheMapper = 
            cachedQuery cconn expr key () dbMapper dbToCache cacheMapper
    uncachedQuery   :: (PG.ToRow q, PG.FromRow r) => cconn -> PG.Query -> q -> IO [r]
    uncachedExecute :: (PG.ToRow q) => cconn -> PG.Query -> q -> IO Int64

instance Connection PG.Connection where
    query = PG.query
    query_ = PG.query_
    execute = PG.execute
    formatQuery = PG.formatQuery

data PgAndRedis c = PgAndRedis c Rd.Connection
-- no need to pool the redis connection as the library seems to do the pooling in that case
data Logged a = Logged FL.TimedFastLogger a 

instance (Connection conn) => Connection (Logged conn)
    where query (Logged logger conn) expr param = do
            str <- formatQuery conn expr param
            logger $ \time -> mconcat [FL.toLogStr time, " -- ", FL.toLogStr str, "\n"]
            query conn expr param
          execute (Logged logger conn) expr param = do 
            str <- formatQuery conn expr param
            logger $ \time -> mconcat [FL.toLogStr time, " -- ", FL.toLogStr str, "\n"]
            execute conn expr param
          formatQuery (Logged _ conn) x y = formatQuery conn x y 

instance (Connection conn) => Connection (Pool conn)
  where query pool expr param = 
          withResource pool $ \conn -> query conn expr param 
        query_ pool expr = 
          withResource pool $ \conn -> query_ conn expr
        execute pool expr param = 
          withResource pool $ \conn -> execute conn expr param
        formatQuery pool expr param = 
          withResource pool $ \conn -> formatQuery conn expr param

instance (Connection conn) => CachedConnection (PgAndRedis conn)
    where cachedQuery (PgAndRedis conn rconn) expr key param
            dbMapper dbToCache cacheMapper = _withCacheHandlers rconn key
                (_onCacheHit cacheMapper)
                (_onCacheMiss conn expr param key dbToCache dbMapper)
          uncachedQuery (PgAndRedis conn _) expr param =
            query conn expr param
          uncachedExecute (PgAndRedis conn _) expr param =
            execute conn expr param 

instance (Connection conn) => Connection (PgAndRedis conn)
    where query = uncachedQuery
          execute = uncachedExecute
          formatQuery (PgAndRedis conn _) = formatQuery conn 

instance (Connection conn) => CachedConnection (Logged (PgAndRedis conn))
    where cachedQuery cconn@(Logged logger (PgAndRedis conn rconn)) expr key
            param dbMapper dbToCache cacheMapper = _withCacheHandlers rconn key
                (\r -> do
                    let str = "Cache hit: " <> key
                    liftIO . logger $ 
                        \time -> mconcat [FL.toLogStr time, " -- ", FL.toLogStr str, 
                            "\n"]
                    _onCacheHit cacheMapper r)
                (\e -> do 
                    let str = "Cache miss: " <> key
                    liftIO . logger $ 
                        \time -> mconcat [FL.toLogStr time, " -- ", FL.toLogStr str, 
                            "\n"]
                    _onCacheMiss conn expr param key dbToCache dbMapper e)
          uncachedQuery (Logged logger (PgAndRedis conn _)) expr param =
            query (Logged logger conn) expr param
          uncachedExecute (Logged logger (PgAndRedis conn _)) expr param =
            execute (Logged logger conn) expr param

mkCached :: (Connection c) => c -> Rd.Connection -> PgAndRedis c
mkCached = PgAndRedis

mkLogged :: a -> IO (Logged a)
mkLogged conn = do 
    timeCache <- newTimeCache simpleTimeFormat
    let ltype = (FL.LogStdout FL.defaultBufSize)
    -- second value is the cleanup action, we can ignore it safrly
    (logger, _) <- FL.newTimedFastLogger timeCache ltype 
    return $ Logged logger conn

mkPostgresPool :: BS.ByteString -> IO (Pool PG.Connection)
mkPostgresPool connStr = createPool (PG.connectPostgreSQL connStr)
  PG.close
  2 -- stripes
  60 -- unused connections are kept open for a minute
  10 -- max. 10 connections open per stripe

_onCacheHit :: (Ae.FromJSON t) =>  (t -> u) -> Rd.Reply -> Rd.Redis u
_onCacheHit cacheMapper reply = do
    let val = (Rd.decode reply) :: Either Rd.Reply BS.ByteString
    case val of 
        Left err  -> fail $ "redis error"
        Right res -> return . cacheMapper . fromJust . 
            Ae.decode . BSL.fromStrict $ res

_onCacheMiss :: (Connection conn, PG.ToRow q, PG.FromRow r, Ae.ToJSON s) => conn -> 
    PG.Query -> q -> CacheKey -> ([r] -> Maybe s) -> ([r] -> u) -> BS.ByteString -> 
    Rd.Redis u
_onCacheMiss conn expr param key dbToCache dbMapper _ = do
    dbres <- liftIO $ query conn expr param
    let mapped = Ae.encode <$> dbToCache dbres
    case mapped of 
        Nothing    -> return ()
        (Just val) -> do 
            _ <- Rd.set key . BSL.toStrict $ val
            return ()
    return $ dbMapper dbres

_withCacheHandlers :: Rd.Connection -> CacheKey -> (Rd.Reply -> Rd.Redis u) ->
    (BS.ByteString -> Rd.Redis u) -> IO u
_withCacheHandlers rconn key
    onHit onMiss = Rd.runRedis rconn $ do
        -- Rd.get :: ByteString -> Redis (Either Reply (Maybe ByteString))
        rres <- Rd.get key
        case rres of 
            Left reply -> onHit reply
            Right err  -> onMiss (maybe "" id err)
