{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module JSend
  ( JSend
  , jsendSucc
  , jsendErr
  , jsendErrWithCode 
  ) where

import qualified Data.Text as T
import qualified Data.Aeson as Ae
import           Data.Swagger (ToSchema)
import           GHC.Generics (Generic)

data JSend a = 
    JSendSuccess a
  | JSendError (Maybe Integer) T.Text
  deriving (Eq, Show, Generic)

instance (Ae.ToJSON a) => Ae.ToJSON (JSend a)
    where toJSON (JSendSuccess val) =  
            Ae.object [("status", "success"),  ("data", Ae.toJSON val)]
          toJSON (JSendError Nothing msg) =
            Ae.object [("status", "error"), ("message", Ae.String msg)]
          toJSON (JSendError (Just code) msg) =
            Ae.object [("status", "error"), ("message", Ae.String msg), 
                ("code", Ae.toJSON code)]

instance (ToSchema a) => ToSchema (JSend a)

jsendErr :: T.Text -> JSend a
jsendErr msg = JSendError Nothing msg 

jsendErrWithCode :: Integer -> T.Text -> JSend a
jsendErrWithCode code msg = JSendError (Just code) msg 

jsendSucc :: a -> JSend a
jsendSucc = JSendSuccess