{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE TypeOperators       #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleContexts    #-}

module Lib (
    startApp
  , Configuration(..) 
  , defaultConfiguration
  ) where

import           API
import           Crypto.JOSE (JWK)
import qualified Data.ByteString as BS
import           Data.Swagger (Swagger)
import qualified Data.Text as T
import qualified Database.Redis as RD (parseConnectInfo, connect)
import qualified DB
import           Network.Wai.Handler.Warp 
                 (runSettings, setPort, setLogger, defaultSettings)
import qualified Network.HTTP.Client as HTTP
import qualified Network.HTTP.Client.TLS as HTTP
import           Network.Wai.Logger (withStdoutLogger)
import           Server (mkServer, mkNoAuthServer)
import           Servant
import           Servant.Auth.Server
import           Servant.Auth.Swagger ()
import           Servant.Swagger (HasSwagger, toSwagger)
import           System.Directory (doesFileExist)
import           System.IO (FilePath)
import           Types (UserId)

data Configuration = Configuration {
    confPort         :: Int
  , confConnStr      :: BS.ByteString
  , confRedisConnStr :: T.Text
  , confKeyPath      :: FilePath
  , confDoHttp       :: Bool 
  , confGodPassword  :: Maybe BS.ByteString
  , confHttpSettings :: HTTP.ManagerSettings
}

defaultConfiguration :: Configuration
defaultConfiguration = Configuration {
    confPort         = 5000
  , confConnStr      = "postgresql://localhost/servant_thing"
  , confRedisConnStr = "redis://localhost:6379"
  , confKeyPath      = "jwtkey.pem"
  , confDoHttp       = True
  , confGodPassword  = Nothing
  , confHttpSettings = HTTP.tlsManagerSettings
}

-- our api except the documentation endpoint. That is what swagger actually describes
-- (The type system, understandably, goes a little bonkers when we try to documrnt
-- the documentation endpoint)
type NoDocAPI = NoAuthAPI :<|> (Auth '[JWT] UserId :> API)

type Documented a = a :<|> ("swagger.json" :> Get '[JSON] Swagger)

-- essentially a convenience type alias for our all-frills-included api
type FullAPI = Documented NoDocAPI

serveWithDocs :: (HasSwagger a) => Server a -> Proxy a -> Server (Documented a)
serveWithDocs server proxy = server :<|> serveSwagger
    where serveSwagger = return $ toSwagger proxy

-- Just couldn't get this function to be polymorphic
serverWithAuth :: (a -> Server API) -> Server (Auth '[JWT] a :> API)
serverWithAuth server' (Authenticated x) = (server' x)
serverWithAuth _ _                       = throwAll err401

serveAuthAndDoc :: Server NoAuthAPI -> (UserId -> Server API) -> Server FullAPI
serveAuthAndDoc noAuthServer apiServer = serveWithDocs serveAuth
    (Proxy :: Proxy NoDocAPI)
    where serveAuth :: Server NoDocAPI
          serveAuth = noAuthServer :<|> (serverWithAuth apiServer)

readOrCreateKey :: FilePath -> IO JWK
readOrCreateKey path = do
    keyExists <- doesFileExist path
    if keyExists
        then readKey path
        else do
            -- generates key and writes to file
            writeKey path
            readKey path

startApp :: Configuration -> IO ()
startApp conf = do
    dbconn <- DB.mkPostgresPool (confConnStr conf)
    let redisConnInfo = RD.parseConnectInfo (T.unpack . confRedisConnStr $ conf)
    rconn <- case redisConnInfo of 
        Left err   -> fail $ "Invalid redis connection string: " <> err
        Right info -> RD.connect info
    let cconn' = DB.mkCached dbconn rconn
    cconn <- DB.mkLogged cconn'
    key <- readOrCreateKey (confKeyPath conf)
    http <- HTTP.newManager (confHttpSettings conf)
    let jwtCfg       = defaultJWTSettings key
        ctx          = defaultCookieSettings :. jwtCfg :. EmptyContext
        noAuthServer = mkNoAuthServer http cconn jwtCfg (confDoHttp conf) (confGodPassword conf)
        fullServer   = serveAuthAndDoc (noAuthServer) (mkServer cconn)
        app          = serveWithContext (Proxy :: Proxy FullAPI) ctx fullServer
    withStdoutLogger $ \apacheLogger -> do
        let settings = setPort (confPort conf) $ setLogger apacheLogger defaultSettings
        putStrLn $ mconcat ["Serving at port ", show $ confPort conf, "..."]
        runSettings settings app
