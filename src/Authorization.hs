module Authorization
    ( editableUser
    , editableBoard
    , createableBoard
    , editableItem
    , createableItem
    ) where

import qualified DB (Connection)
import           Types
import qualified DataAccess as DA 

editableUser :: UserId -> UserId -> Permission (Editable UserId)
editableUser loggedIn userId
    | loggedIn == userId = Able (Editable userId)
    | otherwise          = Unable

editableBoard :: (DB.Connection conn) => conn -> UserId -> BoardId
    -> IO (Permission (Editable BoardId))
editableBoard conn userId boardId = do
    maybeUserFromDb <- DA.getBoardOwner conn boardId
    return $ case maybeUserFromDb of
        Nothing -> Invalid
        (Just userFromDb)
            | userId /= userFromDb -> Unable
            | otherwise            -> Able (Editable boardId)

createableBoard :: (DB.Connection conn) => conn -> UserId -> BoardInput 
    -> IO (Permission (Creatable BoardInput))
createableBoard conn userId boardInput = do 
    userExists <- DA.activeUserExists conn userId
    return $ if (not userExists)
        then Invalid
        else Able . Creatable $ boardInput { boardInputUserId = userId }

editableItem :: (DB.Connection conn) => conn -> UserId -> ItemId
    -> IO (Permission (Editable ItemId))
editableItem conn userId itemId = do
    maybeUserFromDb <- DA.getItemOwner conn itemId
    return $ case maybeUserFromDb of
        Nothing -> Invalid
        (Just userFromDb)
            | userId /= userFromDb -> Unable
            | otherwise            -> Able (Editable itemId)

createableItem :: (DB.Connection conn) => conn -> UserId -> ItemInput
    -> IO (Permission (Creatable ItemInput))
createableItem conn userId itemInput = do
    maybeUserFromDb <- DA.getBoardOwner conn (itemInputBoardId itemInput)
    return $ case maybeUserFromDb of
        Nothing -> Invalid
        (Just userFromDb)
            | userId /= userFromDb -> Unable
            | otherwise            -> Able (Creatable itemInput)
