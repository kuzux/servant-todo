{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DataKinds                  #-}

module Types (
    Permission(..)
  , Editable(..)
  , Creatable(..)

  , UserId
  , BoardId
  , ItemId

  , User
  , Board
  , Item

  , AuthProvider(..)

  , UserInput(..)
  , BoardInput(..)
  , ItemInput(..)
  , LoginInput(..)
  ) where

import           Data.Aeson ((.=), (.:))
import qualified Data.Aeson as Ae
import qualified Data.Text as T
import           Data.Time (LocalTime)
import           Data.UUID.Types (UUID)
import           Data.Swagger (ToSchema, ToParamSchema)
-- kind of annoying to import each class separately
import qualified Database.PostgreSQL.Simple.ToField as PG
import qualified Database.PostgreSQL.Simple.FromField as PG
import qualified Database.PostgreSQL.Simple.FromRow as PG
import           Servant.Auth.Server (FromJWT, ToJWT)
import           GHC.Generics (Generic)
import qualified Web.Internal.HttpApiData as HAD

-- as an example:
-- Invalid = No such board exists
-- Unable  = User can't edit the board 
data Permission a =
    Invalid
  | Unable
  | Able a

newtype Editable a = Editable a
newtype Creatable a = Creatable a

newtype UserId = UserId UUID
    deriving (Eq, Show, PG.FromField, PG.ToField, HAD.FromHttpApiData,
        Ae.FromJSON, Ae.ToJSON, ToSchema, ToParamSchema)
instance FromJWT UserId
instance ToJWT UserId

newtype BoardId = BoardId UUID
    deriving (Eq, Show, PG.FromField, PG.ToField, HAD.FromHttpApiData,
        Ae.FromJSON, Ae.ToJSON, ToSchema, ToParamSchema)

newtype ItemId = ItemId UUID
    deriving (Eq, Show, PG.FromField, PG.ToField, HAD.FromHttpApiData,
        Ae.FromJSON, Ae.ToJSON, ToSchema, ToParamSchema)

data User = User 
    { userId :: UserId
    , userName :: T.Text 
    , userEmail :: T.Text 
    , userCreatedAt :: LocalTime }
    deriving (Generic)

instance PG.FromRow User where
    fromRow = User <$> PG.field <*> PG.field <*> PG.field <*> PG.field

instance Ae.ToJSON User where
    toJSON user = Ae.object [
        "id"         .= (Ae.toJSON $ userId user),
        "name"       .= (Ae.toJSON $ userName user),
        "email"      .= (Ae.toJSON $ userEmail user),
        "created_at" .= (Ae.toJSON $ userCreatedAt user) ]

instance ToSchema User

data Board = Board 
    { boardId        :: BoardId
    , boardName      :: T.Text
    , boardUserId    :: UserId
    , boardCreatedAt :: LocalTime }
    deriving (Generic)

instance PG.FromRow Board where
    fromRow = Board <$> PG.field <*> PG.field <*> PG.field <*> PG.field

instance Ae.ToJSON Board where
    toJSON board = Ae.object [
        "id"         .= (Ae.toJSON $ boardId board),
        "name"       .= (Ae.toJSON $ boardName board),
        "user_id"    .= (Ae.toJSON $ boardUserId board),
        "created_at" .= (Ae.toJSON $ boardCreatedAt board) ]

instance ToSchema Board

data Item = Item 
    { itemId        :: ItemId
    , itemName      :: T.Text 
    , itemCompleted :: Bool 
    , itemBoardId   :: BoardId
    , itemCreatedAt :: LocalTime }
    deriving (Generic)

instance PG.FromRow Item where
    fromRow = Item <$> PG.field <*> PG.field <*> PG.field <*> PG.field 
        <*> PG.field

instance Ae.ToJSON Item where
    toJSON item = Ae.object [
        "id"         .= (Ae.toJSON $ itemId item),
        "name"       .= (Ae.toJSON $ itemName item),
        "completed"  .= (Ae.toJSON $ itemCompleted item),
        "board_id"   .= (Ae.toJSON $ itemBoardId item),
        "created_at" .= (Ae.toJSON $ itemCreatedAt item) ]

instance ToSchema Item

data UserInput = UserInput 
    { userInputName      :: T.Text
    , userInputEmail     :: T.Text }
    deriving (Generic)

instance Ae.FromJSON UserInput where
    parseJSON = Ae.withObject "user" $ \obj ->
        UserInput <$> (obj .: "name") <*> (obj .: "email")

instance ToSchema UserInput

data BoardInput = BoardInput 
    { boardInputName  :: T.Text
    , boardInputUserId :: UserId }
    deriving (Generic)

instance Ae.FromJSON BoardInput where
    parseJSON = Ae.withObject "board" $ \obj ->
        BoardInput <$> (obj .: "name") <*> (obj .: "user_id")

instance ToSchema BoardInput

data ItemInput = ItemInput 
    { itemInputContent   :: T.Text
    , itemInputCompleted :: Bool 
    , itemInputBoardId   :: BoardId }
    deriving (Generic)

instance Ae.FromJSON ItemInput where
    parseJSON = Ae.withObject "item" $ \obj ->
        ItemInput <$> (obj .: "name") <*> (obj .: "completed")
            <*> (obj .: "board_id")

instance ToSchema ItemInput

data AuthProvider = GoogleAuth
    deriving (Generic)

instance Ae.FromJSON AuthProvider where
    parseJSON (Ae.String "google") = return GoogleAuth
    parseJSON _                    = fail "Invalid auth provider"

instance ToSchema AuthProvider

data LoginInput = LoginInput
    { loginInputToken :: T.Text
    , loginInputProvider :: AuthProvider
    , loginInputUserId :: UserId }
    deriving (Generic)

instance Ae.FromJSON LoginInput where
    parseJSON = Ae.withObject "board" $ \obj ->
        LoginInput <$> (obj .: "token") <*> (obj .: "provider")
            <*> (obj .: "user_id")

instance ToSchema LoginInput
