{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

-- does what a dao does, really
module DataAccess
    ( getBoardOwner
    , getItemOwner

    , userExists
    , boardExists
    , activeUserExists
    , activeBoardExists

    , getAllUsers
    , getUserById
    , getUserEmailById
    , getUserByEmail
    , addUser
    , deleteUser

    , getBoardsByUser
    , getBoardById
    , addBoard
    , updateBoard
    , deleteBoard

    , getItemsByBoard
    , getItemsByUser
    , getItemById
    , addItem
    , updateItem
    , deleteItem
    ) where

import           Control.Exception (handleJust)
import           Data.Maybe (listToMaybe, fromJust)
import qualified Data.Text as T
import qualified Database.PostgreSQL.Simple as PG
import qualified Database.PostgreSQL.Simple.Errors as PG
import qualified Database.PostgreSQL.Simple.FromField as PG
import           DB
import           Text.RawString.QQ
import           Types

getBoardOwner :: (DB.Connection conn) => conn -> BoardId -> IO (Maybe UserId)
getBoardOwner conn boardId = querySingleValue conn [r| select boards.user_id from boards 
    where boards.id = ? |] (PG.Only boardId)

getItemOwner :: (DB.Connection conn) => conn -> ItemId -> IO (Maybe UserId)
getItemOwner conn itemId = querySingleValue conn [r| select boards.user_id
    from items join boards on items.board_id = boards.id
    where items.id = ? |] (PG.Only itemId)

getAllUsers :: (DB.Connection conn) => conn -> IO [User]
getAllUsers conn = DB.query_ conn [r|
    select id, name, email, created_at
    from users where deleted_at is null |]

getUserById :: (DB.Connection conn) => conn -> UserId -> IO (Maybe User)
getUserById conn userId = querySingleRow conn [r|
    select id, name, email, created_at
    from users where id = ? and deleted_at is null |] (PG.Only userId)

getUserEmailById :: (DB.Connection conn) => conn -> UserId -> IO (Maybe T.Text)
getUserEmailById conn userId = querySingleValue conn [r| select email
    from users where id = ? and deleted_at is null |] (PG.Only userId)

getUserByEmail :: (DB.Connection conn) => conn -> T.Text -> IO (Maybe User)
getUserByEmail conn email = querySingleRow conn [r|
      select id, name, email, created_at
      from users where email = ?|] (PG.Only email)

addUser :: (DB.Connection conn) => conn -> T.Text -> T.Text -> IO (Maybe UserId)
addUser conn email name = insertAndCatchInvalid conn [r| insert into users
    (name, email) values (?, ?) returning id |] (name, email)

deleteUser :: (DB.Connection conn) => conn -> Editable UserId -> IO Bool
deleteUser conn (Editable userId) = updateAtLeastOne conn [r| update users
        set deleted_at = now() where id = ? |] (PG.Only userId)

getBoardsByUser :: (DB.Connection conn) => conn -> UserId -> IO [Board]
getBoardsByUser conn userId = DB.query conn [r|
    select id, name, user_id, created_at from boards
    where boards.user_id = ? and boards.deleted_at is null |] (PG.Only userId)

getBoardById :: (DB.Connection conn) => conn -> BoardId -> IO (Maybe Board)
getBoardById conn boardId = querySingleRow conn [r|
    select id, name, user_id, created_at from boards
    where boards.id = ? boards.deleted_at is null |] (PG.Only boardId)

addBoard :: (DB.Connection conn) => conn -> Creatable BoardInput
    -> IO (Maybe BoardId)
addBoard conn (Creatable boardInput) = insertAndCatchInvalid conn [r|
    insert into boards (name, user_id) values (?, ?) returning id |]
    (boardInputName boardInput, boardInputUserId boardInput)

updateBoard :: (DB.Connection conn) => conn -> Editable BoardId -> BoardInput
    -> IO Bool
updateBoard conn (Editable boardId) boardInput = updateAtLeastOne conn [r|
    update boards set name = ? where id = ? |]
    (boardInputName boardInput, boardId)

deleteBoard :: (DB.Connection conn) => conn -> Editable BoardId -> IO Bool
deleteBoard conn (Editable boardId) = updateAtLeastOne conn [r| update boards
        set deleted_at = now() where id = ? |] (PG.Only boardId)

getItemsByBoard :: (DB.Connection conn) => conn -> BoardId -> IO [Item]
getItemsByBoard conn boardId = DB.query conn [r| 
    select items.id, items.content, items.completed, items.board_id,
        items.created_at
    from items where items.board_id = ? and items.deleted_at is null |]
    (PG.Only boardId) 

getItemsByUser :: (DB.Connection conn) => conn -> UserId -> IO [Item]
getItemsByUser conn userId = DB.query conn [r| 
    select items.id, items.content, items.completed, items.board_id,
        items.created_at
    from items join boards on items.board_id = boards.id
    where boards.user_id = ? and items.deleted_at is null |] (PG.Only userId) 

getItemById :: (DB.Connection conn) => conn -> ItemId -> IO (Maybe Item)
getItemById conn itemId = querySingleRow conn [r| 
    select items.id, items.content, items.completed, items.board_id,
        items.created_at
    from items where items.id = ? and items.deleted_at is null |] (PG.Only itemId) 

addItem :: (DB.Connection conn) => conn -> Creatable ItemInput
    -> IO (Maybe ItemId)
addItem conn (Creatable itemInput) = insertAndCatchInvalid conn [r| insert into items 
    (board_id, content) values (?, ?) returning id |] 
    (itemInputBoardId itemInput, itemInputContent itemInput)

updateItem :: (DB.Connection conn) => conn -> Editable ItemId -> ItemInput
    -> IO Bool
updateItem conn (Editable itemId) itemInput = updateAtLeastOne conn [r|
    update items set content = ?, completed = ? where id = ? |]
    (itemInputContent itemInput, itemInputCompleted itemInput, itemId)

deleteItem :: (DB.Connection conn) => conn -> Editable ItemId -> IO Bool
deleteItem conn (Editable itemId) = updateAtLeastOne conn [r| update items
    set deleted_at = now() where id = ? |] (PG.Only itemId)

userExists :: (DB.Connection conn) => conn -> UserId -> IO Bool
userExists conn userId = queryRowExists conn [r| select count(*) 
    from users where id = ? |] (PG.Only userId)

boardExists :: (DB.Connection conn) => conn -> BoardId -> IO Bool
boardExists conn boardId = queryRowExists conn [r| select count(*) 
    from boards where id = ? |] (PG.Only boardId)

activeUserExists :: (DB.Connection conn) => conn -> UserId -> IO Bool
activeUserExists conn userId = queryRowExists conn [r| select count(*)  
    from users where id = ? and deleted_at is null |] (PG.Only userId)

activeBoardExists :: (DB.Connection conn) => conn -> BoardId -> IO Bool
activeBoardExists conn boardId = queryRowExists conn [r| select count(*)  
    from boards where id = ? and deleted_at is null |] (PG.Only boardId) 

updateAtLeastOne :: (DB.Connection conn, PG.ToRow q) => conn -> PG.Query
    -> q -> IO Bool
updateAtLeastOne conn expr param = do 
    rows <- DB.execute conn expr param
    return $ (rows > 0)

insertAndCatchInvalid :: (DB.Connection conn, PG.ToRow q, PG.FromField r) => 
    conn -> PG.Query -> q -> IO (Maybe r)
insertAndCatchInvalid conn expr param = handleJust PG.constraintViolation
    handler $ querySingleValue conn expr param
    where handler _ = return Nothing

querySingleValue :: (DB.Connection conn, PG.ToRow q, PG.FromField r) =>
    conn -> PG.Query -> q -> IO (Maybe r)
querySingleValue conn expr param = (fmap PG.fromOnly) <$> querySingleRow conn expr param

querySingleRow :: (DB.Connection conn, PG.ToRow q, PG.FromRow r) =>
    conn -> PG.Query -> q -> IO (Maybe r)
querySingleRow conn expr param = listToMaybe <$> DB.query conn expr param

-- only use with count queries
queryRowExists :: (DB.Connection conn, PG.ToRow q) => conn -> PG.Query -> q
    -> IO Bool 
queryRowExists conn expr param = do 
    dbres <- fromJust <$> querySingleValue conn expr param :: IO Int 
    return $ dbres > 0
