{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE TemplateHaskell     #-}

module Server 
  ( mkServer 
  , mkNoAuthServer ) where 

import qualified Authentication as Auth
import qualified Authorization as Auth
import           API
import           Control.Monad.IO.Class (liftIO)
import qualified Data.ByteString as BS
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified DataAccess as DA
import qualified DB
import           JSend
import qualified Network.HTTP.Client as HTTP
import           Servant
import           Servant.Auth.Server (JWTSettings)
import           Types

-- these are required for the commented out instance
-- import           Control.Monad.Fail (MonadFail, fail)
-- import qualified Data.ByteString.Lazy.UTF8 as BSLU

-- we can't directly match in the handler do blocks without that definition
-- nice to have for development purposes
{- instance MonadFail Handler
    where fail msg = throwError $ err500 { errBody = BSLU.fromString msg } -}

mkServer :: (DB.CachedConnection conn) => conn -> UserId -> Server API
mkServer conn userId = do 
    let serveUsers  = (getUsers conn) :<|> (getUser conn)
                  :<|> (userIdFromEmail conn) :<|> (deleteUser conn userId)
    let serveBoards = (getBoard conn) :<|> (getBoards conn) :<|> (createBoard conn userId) 
                  :<|> (updateBoard conn userId) :<|> (deleteBoard conn userId)
    let serveItems  = (getItem conn) :<|> (getItems conn) :<|> (getItemsByUser conn)
                  :<|> (addItem conn userId) :<|> (updateItem conn userId)
                  :<|> (deleteItem conn userId)
    serveUsers :<|> serveBoards :<|> serveItems

mkNoAuthServer :: (DB.Connection conn) => HTTP.Manager -> conn -> 
    JWTSettings -> Bool -> Maybe BS.ByteString -> Server NoAuthAPI
mkNoAuthServer http conn jwtCfg doHttp godPwd = 
    (login http conn jwtCfg doHttp godPwd) :<|> (createUser conn)

login :: (DB.Connection conn) => HTTP.Manager -> conn -> JWTSettings -> 
    Bool -> Maybe BS.ByteString -> LoginInput -> Handler T.Text
login http conn jwtCfg doReq godPwd loginInput = do
    let maybeHttp = if doReq then Just http else Nothing
    authUid <- liftIO $ Auth.authMethods [Auth.godPwdAuth godPwd, 
                                          Auth.googleAuth maybeHttp conn] 
                                         loginInput
    tok <- liftIO $ Auth.issueToken jwtCfg authUid
    return $ either (\_ -> "Unable to issue token") T.decodeUtf8 tok

getUsers :: (DB.Connection conn) => conn -> Handler (JSend [User])
getUsers conn = do
    users <- liftIO $ DA.getAllUsers conn
    return $ jsendSucc users 

getUser :: (DB.Connection conn) => conn -> UserId -> Handler (JSend User)
getUser conn userId = do
    dbres <- liftIO $ DA.getUserById conn userId
    return $ maybe (jsendErr "User Not Found") jsendSucc dbres

createUser :: (DB.Connection conn) => conn -> UserInput -> Handler (JSend UserId)
createUser conn userToAdd = do
    dbres <- liftIO $ DA.addUser conn (userInputEmail userToAdd)
        (userInputName userToAdd)
    return $ maybe (jsendErr "Could not insert into database") jsendSucc
        dbres

userIdFromEmail :: (DB.Connection conn) => conn -> T.Text -> Handler (JSend User)
userIdFromEmail conn mail = do
    dbres <- liftIO $ DA.getUserByEmail conn mail
    return $ maybe (jsendErr "User Not Found") jsendSucc dbres

deleteUser :: (DB.Connection conn) => conn -> UserId -> UserId
    -> Handler (JSend ())
deleteUser conn userLoggedIn userToDelete = do
    let canEdit = Auth.editableUser userLoggedIn userToDelete
    case canEdit of
        Invalid       -> return $ jsendErr "User Not Found"
        Unable        -> return $
            jsendErr "You are not authorized to perform this operation"
        Able editable -> do
            dbres <- liftIO $ DA.deleteUser conn editable
            return $ if dbres
                then jsendSucc ()
                else (jsendErr "User Not Found")

getBoard :: (DB.Connection conn) => conn -> BoardId -> Handler (JSend Board)
getBoard conn boardId = do 
    dbres <- liftIO $ DA.getBoardById conn boardId
    return $ maybe (jsendErr $ "Board not found") jsendSucc dbres

getBoards :: (DB.Connection conn) => conn -> UserId
    -> Handler (JSend [Board])
getBoards conn userId = do 
    exists <- liftIO $ DA.userExists conn userId
    if exists
        then do
            boards <- liftIO $ DA.getBoardsByUser conn userId 
            return . jsendSucc $ boards
        else return (jsendErr "User Not Found")

createBoard :: (DB.Connection conn) => conn -> UserId -> BoardInput
    -> Handler (JSend BoardId)
createBoard conn userId boardInput = do
    canCreate <- liftIO $ Auth.createableBoard conn userId boardInput
    case canCreate of
        Invalid -> return $ jsendErr "No such user"
        Unable  -> return $
            jsendErr "You are not authorized to perform this operation"
        Able creatable -> do
            dbres <- liftIO $ DA.addBoard conn creatable
            return $ maybe (jsendErr "Could not insert into database")
                jsendSucc dbres

updateBoard :: (DB.Connection conn) => conn -> UserId -> BoardId
    -> BoardInput -> Handler (JSend ())
updateBoard conn userId boardId boardInput = do
    canEdit <- liftIO $ Auth.editableBoard conn userId boardId
    case canEdit of
        Invalid              -> return $ jsendErr "User/Board does not exist"
        Unable               ->
            return $ jsendErr "You are not authorized to perform this operation"
        Able editableBoardId -> do
            deleted <- liftIO $ DA.updateBoard conn editableBoardId boardInput
            return $ if deleted
                then jsendSucc ()
                else jsendErr "Unable to delete board"

deleteBoard :: (DB.Connection conn) => conn -> UserId -> BoardId
    -> Handler (JSend ())
deleteBoard conn userId boardId = do
    canEdit <- liftIO $ Auth.editableBoard conn userId boardId
    case canEdit of
        Invalid              -> return $ jsendErr "User/Board does not exist"
        Unable               ->
            return $ jsendErr "You are not authorized to perform this operation"
        Able editableBoardId -> do
            deleted <- liftIO $ DA.deleteBoard conn editableBoardId
            return $ if deleted
                then jsendSucc ()
                else jsendErr "Unable to delete board"

getItem :: (DB.Connection conn) => conn -> ItemId -> Handler (JSend Item)
getItem conn itemId = do 
    dbres <- liftIO $ DA.getItemById conn itemId
    return $ maybe (jsendErr $ "Item not found") jsendSucc dbres

getItems :: (DB.Connection conn) => conn -> BoardId -> Handler (JSend [Item])
getItems conn boardId = do
    exists <- liftIO $ DA.boardExists conn boardId
    if exists
    then do
        items <- liftIO $ DA.getItemsByBoard conn boardId
        return . jsendSucc $ items
    else return $ jsendErr "No such board"

getItemsByUser :: (DB.Connection conn) => conn -> UserId -> Handler (JSend [Item])
getItemsByUser conn userId = do
    exists <- liftIO $ DA.userExists conn userId
    if exists
    then do
        items <- liftIO $ DA.getItemsByUser conn userId
        return . jsendSucc $ items
    else return $ jsendErr "No such board"

addItem :: (DB.Connection conn) => conn -> UserId -> ItemInput
    -> Handler (JSend ItemId)
addItem conn userId itemInput = do
    canAdd <- liftIO $ Auth.createableItem conn userId itemInput
    case canAdd of
        Invalid        -> return $ jsendErr "User/Board does not exist"
        Unable         -> return $
            jsendErr "You are not authorized to perform this operation"
        Able itemToAdd -> do
            dbres <- liftIO $ DA.addItem conn itemToAdd
            return $ maybe (jsendErr "Could not insert into database")
                jsendSucc dbres

updateItem :: (DB.Connection conn) => conn -> UserId -> ItemId
    -> ItemInput -> Handler (JSend ())
updateItem conn userId itemId itemInput = do
    canEdit <- liftIO $ Auth.editableItem conn userId itemId
    case canEdit of
        Invalid -> return $ jsendErr "User/Item does not exist"
        Unable  -> return $
            jsendErr "You are not authorized to perform this operation"
        Able editableItem -> do
            updated <- liftIO $ DA.updateItem conn editableItem itemInput
            return $ if updated
                then jsendSucc ()
                else jsendErr "Unable to update item"

deleteItem :: (DB.Connection conn) => conn -> UserId -> ItemId 
    -> Handler (JSend ())
deleteItem conn userId itemId = do
    canEdit <- liftIO $ Auth.editableItem conn userId itemId
    case canEdit of
        Invalid -> return $ jsendErr "User/Item does not exist"
        Unable  -> return $
            jsendErr "You are not authorized to perform this operation"
        Able editableItem -> do
            deleted <- liftIO $ DA.deleteItem conn editableItem
            return $ if deleted
                then jsendSucc ()
                else jsendErr "Unable to delete item"
