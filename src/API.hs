{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}

module API 
  ( NoAuthAPI 
  , API
  ) where

import qualified Data.Text as T
import           JSend
import           Servant
import           Types
          
type UsersAPI =  Get '[JSON] (JSend [User])
            :<|> Capture "id" UserId :> Get '[JSON] (JSend User)
            :<|> "by-email" :> Capture "email" T.Text 
                :> Get '[JSON] (JSend User)
            :<|> Capture "id" UserId :> Delete '[JSON] (JSend ())

type BoardsAPI = Capture "board-id" BoardId :> Get '[JSON] (JSend Board)
             :<|> "by-user" :> Capture "user-id" UserId 
                :> Get '[JSON] (JSend [Board])
             :<|> ReqBody '[JSON] BoardInput :> Post '[JSON] (JSend BoardId)
             :<|> Capture "board-id" BoardId
                :> ReqBody '[JSON] BoardInput :> Put '[JSON] (JSend ())
             :<|> Capture "board-id" BoardId 
                :> Delete '[JSON] (JSend ())

type ItemsAPI = Capture "item-id" ItemId :> Get '[JSON] (JSend Item)
            :<|> "by-board" :> Capture "board-id" BoardId 
                :> Get '[JSON] (JSend [Item])
            :<|> "by-user" :> Capture "user-id" UserId 
                :> Get '[JSON] (JSend [Item])
            :<|> ReqBody '[JSON] ItemInput :> Post '[JSON] (JSend ItemId)
            :<|> Capture "item-id" ItemId :> ReqBody '[JSON] ItemInput 
                :> Put '[JSON] (JSend ())
            :<|> Capture "item-id" ItemId:> Delete '[JSON] (JSend ())

-- we get a text when we encode the binary blob as base64
type NoAuthAPI = "login" :> ReqBody '[JSON] LoginInput
                :> Post '[PlainText] T.Text
             :<|> "signup" :> ReqBody '[JSON] UserInput 
                :> Post '[JSON] (JSend UserId)

type API =  "users" :> UsersAPI
       :<|> "boards" :> BoardsAPI
       :<|> "items" :> ItemsAPI
