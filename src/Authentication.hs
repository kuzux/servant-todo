{-# LANGUAGE OverloadedStrings   #-}

module Authentication 
    ( Authenticated
    , authMethods
    , godPwdAuth
    , googleAuth
    , issueToken ) where

import qualified Crypto.JOSE.Error as CryptoError
import           Control.Applicative ((<|>), Alternative(..))
import           Control.Monad (liftM)
import           Data.Aeson ((.:))
import qualified Data.Aeson.Types as Ae
import qualified Data.ByteString as BS hiding (empty)
import qualified Data.ByteString.Lazy as BSL hiding (empty)
import           Data.Maybe (fromJust)
import qualified Data.Text as T hiding (empty)
import qualified Data.Text.Encoding as T
import qualified DataAccess as DA
import qualified DB
import qualified Network.HTTP.Client as HTTP
import qualified Network.HTTP.Simple as HTTP
import qualified Network.HTTP.Types.Status as Status
import           Servant.Auth.Server (JWTSettings, makeJWT)
import           Types (LoginInput(..), UserId)

-- authMethods functionn probably exists as a more general combinator
-- it is something like <|>, given an applicative instance for Authenticated a
-- (liftM $ foldl (<|>) empty) . sequence :: t (m (f a)) -> m (f a)
-- which is exactly the type we need given t = [], m = IO, f = Authenticated
-- this thing should not execute more actions than necessary, thanks to lazy evaluation
data Authenticated a = NoAuth | HasAuth a
instance Functor Authenticated where
    fmap _ NoAuth = NoAuth
    fmap f (HasAuth x) = HasAuth (f x)
instance Applicative Authenticated where 
    pure _ = NoAuth
    NoAuth <*> _ = NoAuth
    (HasAuth f) <*> something = fmap f something
instance Alternative Authenticated where
    empty = NoAuth
    (HasAuth x) <|> _ = (HasAuth x)
    NoAuth <|> y = y

authMethods :: [LoginInput -> IO (Authenticated UserId)] -> LoginInput -> IO (Authenticated UserId)
authMethods auths loginInput = totalAction
    where totalAction :: IO (Authenticated UserId)
          totalAction = (liftM $ foldl (<|>) empty) . sequence $ actions
          actions :: [IO (Authenticated UserId)]
          actions = map ($ loginInput) auths

godPwdAuth :: Maybe BS.ByteString -> LoginInput -> IO (Authenticated UserId)
godPwdAuth Nothing _ = return NoAuth
godPwdAuth (Just pwd) loginInput 
    | pwd == inputTok = return . HasAuth $ inputUid
    | otherwise = return NoAuth
    where inputUid = loginInputUserId loginInput
          inputTok = T.encodeUtf8 . loginInputToken $ loginInput

googleAuth :: (DB.Connection conn) => (Maybe HTTP.Manager) -> conn -> 
    LoginInput -> IO (Authenticated UserId)
googleAuth Nothing _ _ = return NoAuth
googleAuth (Just http) conn loginInput = do
    req' <- HTTP.parseRequest "https://oauth2.googleapis.com/tokeninfo"
    let req = HTTP.setQueryString 
                [("id_token", Just . T.encodeUtf8 . loginInputToken $ loginInput)] 
                $ HTTP.setRequestManager http req'
    resp <- HTTP.httpJSON req
    inputMail <- maybe "" id <$> (DA.getUserEmailById conn inputUid)
    let respMail = Ae.parseMaybe (.: "email") (HTTP.getResponseBody resp)
    return $ case (Status.statusCode $ HTTP.responseStatus resp) of
                -- respMail can be Nothing, but it is never so when there 
                -- is a 200 response
                200 | inputMail == (fromJust respMail) -> HasAuth inputUid
                    | otherwise                        -> NoAuth
                _                                      -> NoAuth
    where inputUid = loginInputUserId loginInput
          inputTok = loginInputToken loginInput

issueToken :: JWTSettings -> (Authenticated UserId) -> IO (Either CryptoError.Error BS.ByteString)
issueToken _ NoAuth = return $ Left CryptoError.JWSInvalidSignature
-- TODO: Add some sort of a time limit
issueToken jwtCfg (HasAuth uid) = (BSL.toStrict <$>) <$> makeJWT uid jwtCfg Nothing
