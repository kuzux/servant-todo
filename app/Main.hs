{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Data.HashMap.Lazy as HM
import           Data.Maybe (listToMaybe)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Encoding as T
import           Lib (startApp, defaultConfiguration, Configuration(..))
import           System.Environment (getArgs)
import           System.Exit (exitFailure)
import qualified Text.Parsec.Error as P
import           Text.Toml (parseTomlDoc)
import           Text.Toml.Types (Table, Node(..))

getSettings :: Table -> Either T.Text Configuration
getSettings table = do 
    let conf = defaultConfiguration
    -- TODO: I'm pretty sure this thing can be handled with less repetition
    conf'  <- case "port" `HM.lookup` table of
        Nothing           -> return conf
        Just (VInteger n) -> return conf { confPort = fromIntegral n }
        Just _            -> Left "port has to be an integer"
    conf'' <- case "connStr" `HM.lookup` table of 
        Nothing            -> return conf'
        Just (VString str) -> return conf' { confConnStr = T.encodeUtf8 str }
        _                  -> Left "connStr has to be a string"
    conf3' <- case "redisConnStr" `HM.lookup` table of 
        Nothing            -> return conf''
        Just (VString str) -> return conf'' { confRedisConnStr = str }
        _                  -> Left "redisConnStr has to be a string"
    conf4' <- case "doHttp" `HM.lookup` table of 
        Nothing              -> return conf3'
        Just (VBoolean bool) -> return conf3' { confDoHttp = bool }
        _                    -> Left "doHttp has to be a boolean"
    conf5' <- case "godPassword" `HM.lookup` table of 
        Nothing            -> return conf4'
        Just (VString str) -> return conf4' { confGodPassword = Just $ T.encodeUtf8 str }
        _                  -> Left "godPassword has to be a string"
    return conf5'

main :: IO ()
main = do
    maybeFilename <- listToMaybe <$> getArgs
    case maybeFilename of
        Nothing         -> printUsage
        (Just filename) -> do 
            contents <- T.readFile filename
            let confFile = parseTomlDoc "" contents
            case confFile of
                Left err   -> configParseError err
                Right table -> serveWithConfig table
    where printUsage = do
            putStrLn "Usage: servant-todo-exe config-file"
            exitFailure
          configParseError err = do 
            let str = unlines . (map P.messageString) . P.errorMessages $ err
            putStrLn str 
            exitFailure
          serveWithConfig table = do 
            let conf' = getSettings table
            case conf' of
                Left err -> do 
                    T.putStrLn err 
                    exitFailure
                Right conf -> startApp conf
