#!/usr/bin/env python3
import subprocess as sp

sp.call("""
	ssh -p 2222 arddd@localhost "cd servant-build; git reset --hard; stack build"
""", shell = True)
proc = sp.Popen("""
	ssh -p 2222 arddd@localhost "cd servant-build; stack exec which servant-todo-exe"
""", stdout = sp.PIPE, shell = True)
(filename, _) = proc.communicate()
cmd = """
	scp -P 2222 arddd@localhost:{} ./linux-binary
""".format(str(filename, 'utf-8').strip()).strip()
print(cmd)
sp.call(cmd, shell = True)
